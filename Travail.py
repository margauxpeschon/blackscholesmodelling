from math import sqrt, log, erf, exp
import random
#rajout de std
from numpy import arange, std
# Arange : Return evenly spaced values within a given interval.
import matplotlib.pyplot as plt
import pandas as pd
import os
S0 = 100.0  # S0 = Stock price
strikes = [i for i in range(50, 150)]
#  Exercise prices range
T = 1
#  T = Time to expiration
r = 0.01
#  r = risk-free interest rate
q = 0.02
#  q = dividend yield
vol = 0.2
#  vol = volatility
Nsteps = 100
# Number or steps in MC

my_file1 = "./BlackScholes.csv"
opened_file1 = open(my_file1)
liste1 = []



#données en vert ok
for key, line in enumerate (opened_file1):
    if key != 0 :
        list = line.split(";")
        print(list)
        if len(list[0]) > 1:
            nombre = list[0]
            nombre1 = float(nombre)
        liste1.append(nombre1)

plt.plot(liste1 , 'go')

#faire un scénario où la solution analytique = ligne bleue
# la MC des points rouges et les données vertes + les signe.

#1 the analytical way with back-scholes formula :
listA = []
for strike in strikes:
    d1 = (log(S0/strike) + (r-q + (((vol**2)/2))*T) ) / vol*(sqrt(T))
    d2 = (log(S0/strike) + (r - q - (((vol**2)/2))*T) ) / vol*(sqrt(T))
    C0 = S0 * erf(d1) - strike * exp(-r*T) * erf(d2)
    listA.append(C0)
    print(C0)
plt.plot(listA)


#la fonction N = erf #The erf() function can be used to compute traditional statistical functions such as the cumulative standard normal distribution:

# 2 by a geometric Brownian motion
#vol = sigma
#intervalle de temps = t  = racine de T
t = sqrt(T)
list = []
for key, strike in enumerate(strikes):
    Wt = t*random.uniform(0,1)
    St = S0 * exp((r - q - ((vol**2)/2))*(t*key)+ vol * Wt)
    list.append(St)

    print(St)
plt.plot(list, 'ro')
# Monte Carlo process.


plt.show()